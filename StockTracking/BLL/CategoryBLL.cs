﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockTracking.BLL;
using StockTracking.DAL.DTO;
using StockTracking.DAL;
using StockTracking.DAL.DAO;

namespace StockTracking.BLL
{
    public class CategoryBLL : IBLL<CategoryDetailDTO, CategoryDTO>
    {
        CategoryDAO dao = new CategoryDAO();
        ProductDAO pdao = new ProductDAO();
        SalesDAO sdao = new SalesDAO();

        public bool Delete(CategoryDetailDTO entity)
        {
            CATEGORY dcategory = new CATEGORY();
            dcategory.ID = entity.ID;
            dao.Delete(dcategory);
            if (entity.CategoryName == "" || entity.CategoryName == "  ")
            {
                PRODUCT dproduct = new PRODUCT();
                dproduct.CategoryID = entity.ID;
                pdao.Delete(dproduct);
            }
            if (entity.CategoryName == "" || entity.CategoryName == "   ")
            {
                SALE dsales = new SALE();
                dsales.CategoryID = entity.ID;
                sdao.Delete(dsales);
            }
            return true;
        }

        public bool GetBack(CategoryDetailDTO entity)
        {
            return dao.GetBack(entity.ID);
        }

        public bool Insert(CategoryDetailDTO entity)
        {
            CATEGORY category = new CATEGORY();
            category.CategoryName = entity.CategoryName;
            return dao.Insert(category);
        }

        public CategoryDTO Select()
        {
            CategoryDTO dto = new CategoryDTO();
            dto.Categories = dao.Select();
            return dto;
        }

        public bool Update(CategoryDetailDTO entity)
        {
            CATEGORY catUpdate = new CATEGORY();
            catUpdate.ID = entity.ID;
            catUpdate.CategoryName = entity.CategoryName;
            return dao.Update(catUpdate);
        }
    }
}
