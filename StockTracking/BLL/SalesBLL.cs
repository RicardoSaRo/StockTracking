﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockTracking.DAL;
using StockTracking.DAL.DTO;
using StockTracking.DAL.DAO;

namespace StockTracking.BLL
{
    public class SalesBLL : IBLL<SalesDetailDTO, SalesDTO>
    {
        SalesDAO dao = new SalesDAO();
        CategoryDAO categoryDAO = new CategoryDAO();
        ProductDAO productDAO = new ProductDAO();
        CustomerDAO customerDao = new CustomerDAO();
        public bool Delete(SalesDetailDTO entity)
        {
            SALE ddto = new SALE();
            ddto.ID = entity.ID;
            return  dao.Delete(ddto);
        }

        public bool GetBack(SalesDetailDTO entity)
        {
            return dao.GetBack(entity.ID);
        }

        public bool Insert(SalesDetailDTO entity)
        {
            SALE dto = new SALE();
            dto.ProductID = entity.ProductID;
            dto.CustomerID = entity.CustomerID;
            dto.CategoryID = entity.CategoryID;
            dto.ProductSalesAmount = entity.ProductSalesAmount;
            dto.ProductSalesPrice = entity.ProductSalesPrice;
            dto.SalesDate = entity.SalesDate;
            PRODUCT stockUpdate = new PRODUCT();
            stockUpdate.ID = entity.ProductID;
            stockUpdate.ProductName = entity.ProductName;
            stockUpdate.StockAmount = entity.StockAmount - entity.ProductSalesAmount;
            stockUpdate.Price = entity.ProductSalesPrice;
            productDAO.Update(stockUpdate);
            return dao.Insert(dto);
        }

        public SalesDTO Select()
        {
            SalesDTO dto = new SalesDTO();
            dto.Categories = categoryDAO.Select();
            dto.Products = productDAO.Select();
            dto.Customers = customerDao.Select();
            dto.Sales = dao.Select();
            return dto;
        }

        public SalesDTO Select(bool isDeleted)
        {
            SalesDTO dto = new SalesDTO();
            dto.Categories = categoryDAO.Select(isDeleted);
            dto.Products = productDAO.Select(isDeleted);
            dto.Customers = customerDao.Select(isDeleted);
            dto.Sales = dao.Select(isDeleted);
            return dto;
        }

        public bool Update(SalesDetailDTO entity)
        {
            PRODUCT stockUpdate = new PRODUCT();
            stockUpdate.ID = entity.ProductID;
            stockUpdate.ProductName = entity.ProductName;
            stockUpdate.Price = entity.ProductSalesPrice;
            if (entity.CategoryID != 0) //--> Normal Stock Update
            {
                SALE dto = new SALE();
                dto.ID = entity.ID;
                dto.ProductID = entity.ProductID;
                dto.CustomerID = entity.CustomerID;
                dto.CategoryID = entity.CategoryID;
                dto.ProductSalesAmount = entity.ProductSalesAmount;
                dto.ProductSalesPrice = entity.ProductSalesPrice;
                dto.SalesDate = entity.SalesDate;
                stockUpdate.StockAmount = entity.StockAmount - entity.ProductSalesAmount; //--> Substract from stock
                productDAO.Update(stockUpdate);
                return dao.Update(dto);
            }
            else //--> Gives back the stock amount if the product category is changed
            {
                stockUpdate.StockAmount = entity.StockAmount + entity.ProductSalesAmount; //--> Adds to stock
                return productDAO.Update(stockUpdate);
            }
            
        }
    }
}
