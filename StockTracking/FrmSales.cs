﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StockTracking.BLL;
using StockTracking.DAL.DTO;

namespace StockTracking
{
    public partial class FrmSales : Form
    {
        public FrmSales()
        {
            InitializeComponent();
        }

        private void txtSalesAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = General.isNumber(e);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public SalesDTO dto = new SalesDTO();
        public SalesDetailDTO detail = new SalesDetailDTO();
        public SalesDetailDTO originalDetail = new SalesDetailDTO();
        ProductDetailDTO prodUpdate = new ProductDetailDTO();
        public bool isUpdate = false;
        SalesBLL bll = new SalesBLL();
        CustomerBLL custBll = new CustomerBLL();
        ProductBLL prodBll = new ProductBLL();
        bool comboFull = false;
        bool prodGridFull = false;
        bool custGridFull = false;
        private void FrmSales_Load(object sender, EventArgs e)
        {
            cmbCategory.DataSource = dto.Categories;
            cmbCategory.DisplayMember = "CategoryName";
            cmbCategory.ValueMember = "ID";
            cmbCategory.SelectedIndex = -1;
            comboFull = true;

            gridProduct.DataSource = dto.Products;
            gridProduct.Columns[0].Visible = false; //--> ProductID
            gridProduct.Columns[1].HeaderText = "Product Name";
            gridProduct.Columns[2].Visible = false; //--> CategoryID
            gridProduct.Columns[3].Visible = false; //--> CategoryName
            gridProduct.Columns[4].Visible = false; //--> StockAmount
            gridProduct.Columns[5].Visible = false; //--> Price
            prodGridFull = true;

            gridCostumer.DataSource = dto.Customers;
            gridCostumer.Columns[0].Visible = false;
            gridCostumer.Columns[1].HeaderText = "Customer Name";
            custGridFull = true;

            if (isUpdate)
            {
                txtCustomerName.Text = detail.CustomerName;
                txtProductName.Text = detail.ProductName;
                txtPrice.Text = detail.ProductSalesPrice.ToString();
                txtStock.Text = detail.StockAmount.ToString();
                txtSalesAmount.Text = detail.ProductSalesAmount.ToString();
                btnClose.Text = "Cancel";
                gridProduct.CurrentCell = null;
                gridCostumer.CurrentCell = null;

                //--> This next code can be used if you want to just update the amount
                //cmbCategory.Enabled = false;
                //txtCustomerSearch.Enabled = false;
                //gridProduct.Enabled = false;
                //gridCostumer.Enabled = false;
            }
        }

        private void txtCustomerSearch_TextChanged(object sender, EventArgs e)
        {
            CustomerDTO customerlist = new CustomerDTO();
            customerlist = custBll.Select();
            customerlist.Customers = customerlist.Customers.Where(x => x.CustomerName.ToLower().Contains(txtCustomerSearch.Text.ToLower())).ToList();
            gridCostumer.DataSource = customerlist.Customers;
            if (customerlist.Customers.Count == 0)
            {
                txtSalesAmount.Clear();
                txtCustomerSearch.Clear();
                txtStock.Clear();
            }
        }

        private void cmbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboFull)
            {
                ProductDTO productlist = new ProductDTO();
                productlist = prodBll.Select();
                productlist.Products = productlist.Products.Where(x => x.CategoryID == Convert.ToInt32(cmbCategory.SelectedValue)).ToList();
                gridProduct.DataSource = productlist.Products;
                if (productlist.Products.Count == 0)
                {
                    txtSalesAmount.Clear();
                    txtCustomerSearch.Clear();
                    txtStock.Clear();
                }
            }
        }

        private void gridProduct_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (prodGridFull)
            {
                detail.ProductID = Convert.ToInt32(gridProduct.Rows[e.RowIndex].Cells[0].Value);
                txtProductName.Text = gridProduct.Rows[e.RowIndex].Cells[1].Value.ToString();
                detail.ProductName = txtProductName.Text;
                detail.CategoryID = Convert.ToInt32(gridProduct.Rows[e.RowIndex].Cells[2].Value);
                detail.CategoryName = Convert.ToString(gridProduct.Rows[e.RowIndex].Cells[3].Value);
                txtStock.Text = gridProduct.Rows[e.RowIndex].Cells[4].Value.ToString();
                detail.StockAmount = Convert.ToInt32(txtStock.Text);
                txtPrice.Text = gridProduct.Rows[e.RowIndex].Cells[5].Value.ToString();
                detail.ProductSalesPrice = Convert.ToInt32(txtPrice.Text);
            }
        }

        private void gridCostumer_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (custGridFull)
            {
                detail.CustomerID = Convert.ToInt32(gridCostumer.Rows[e.RowIndex].Cells[0].Value);
                txtCustomerName.Text = gridCostumer.Rows[e.RowIndex].Cells[1].Value.ToString();
                detail.CustomerName = Convert.ToString(txtCustomerName.Text);
            }
        }

        SalesDetailDTO restoreStock(SalesDetailDTO saleProduct)
        {
            //--> Saves Category in an aux var to update just the product in the BLL, then assigns back the original value
            int auxCategory = 0;
            int auxProductSalesAmount = 0;
            ProductDetailDTO auxDTO = new ProductDetailDTO();
            auxCategory = saleProduct.CategoryID;
            auxProductSalesAmount = saleProduct.ProductSalesAmount;
            saleProduct.CategoryID = 0;
            saleProduct.ProductSalesAmount = originalDetail.ProductSalesAmount;
            bll.Update(saleProduct);
            dto = bll.Select();
            //--> IMPORTANT!!!: Add a isDeleted check before the LINQ to prevent exceptions with already deleted products
            auxDTO = dto.Products.First(x => x.ID == saleProduct.ProductID);
            saleProduct.StockAmount = auxDTO.StockAmount;
            saleProduct.CategoryID = auxCategory;
            saleProduct.ProductSalesAmount = auxProductSalesAmount;
            return saleProduct;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtProductName.Text.Trim() == "")
                MessageBox.Show("Please select a product from table.");
            else if (txtCustomerName.Text.Trim() == "")
                MessageBox.Show("Please select a customer from table.");
            else if (txtSalesAmount.Text.Trim() == "")
                MessageBox.Show("Please enter the product sale amount.");
            else if (Convert.ToInt32(txtSalesAmount.Text) > detail.StockAmount)
                MessageBox.Show("Not enough product stock for that sale amount.");
            else
            {
                detail.ProductSalesAmount = Convert.ToInt32(txtSalesAmount.Text);
                if (!isUpdate)
                {
                    detail.SalesDate = DateTime.Today;
                    //detail.ProductSalesPrice *= Convert.ToInt32(txtStock.Text); //--> Sale Total
                    if (bll.Insert(detail))
                    {
                        MessageBox.Show("Sale was added.");
                        bll = new SalesBLL();
                        dto = bll.Select();
                        gridProduct.DataSource = dto.Products;
                        gridCostumer.DataSource = dto.Customers;
                        comboFull = false;
                        cmbCategory.DataSource = dto.Categories;
                        if (dto.Products.Count == 0)
                            comboFull = true;
                        cmbCategory.SelectedIndex = -1;
                        txtSalesAmount.Clear();
                        txtCustomerSearch.Clear();
                    }
                }
                else
                {
                    SalesDTO deletedprods = new SalesDTO();
                    List<ProductDetailDTO> deletecheck = new List<ProductDetailDTO>();
                    deletedprods = bll.Select(true);
                    deletecheck = deletedprods.Products.Where(x => x.ID.Equals(detail.ProductID)).ToList();
                    if (deletecheck.Count > 0) //--> Control to prevent exception when a sales is updated and tries to restock a deleted product.
                        MessageBox.Show("The sale amount cannot be updated if the product is deleted. Get back the deleted product and try again.","Warning!!",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    else
                    {
                        if (detail.CategoryID != originalDetail.CategoryID)
                        {
                            originalDetail = restoreStock(originalDetail);
                        }
                        else
                        {
                            detail = restoreStock(detail);
                        }
                        if (bll.Update(detail))
                        {
                            MessageBox.Show("Sale was updated");
                            this.Close();
                        }
                    }
                }
            }
        }
    }
}
