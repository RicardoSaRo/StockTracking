﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StockTracking.BLL;
using StockTracking.DAL.DTO;
using StockTracking.DAL;


namespace StockTracking
{
    public partial class FrmDeleted : Form
    {
        SalesBLL sbll = new SalesBLL();
        ProductBLL pbll = new ProductBLL();
        CategoryBLL kbll = new CategoryBLL();
        CustomerBLL cbll = new CustomerBLL();
        SalesDTO dto = new SalesDTO();
        SalesDetailDTO detail = new SalesDetailDTO();
        bool gridfull = false;

        public FrmDeleted()
        {
            InitializeComponent();
        }

        private void FrmDeleted_Load(object sender, EventArgs e)
        {
            cmbDeletedData.Items.Add("Category");
            cmbDeletedData.Items.Add("Product");
            cmbDeletedData.Items.Add("Customer");
            cmbDeletedData.Items.Add("Sales");
            dto = sbll.Select(true);
            dataGridView1.DataSource = dto.Sales;
            cmbDeletedData.SelectedItem = "Sales";
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            detail = new SalesDetailDTO();
            switch (cmbDeletedData.SelectedItem)
            {
                case "Category": detail.CategoryID = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value); break;
                case "Product": detail.ProductID = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value); break;
                case "Customer": detail.CustomerID = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value); break;
                case "Sales": detail.ID = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value); break;
                default: break;
            }
        }

        private void cmbDeletedData_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cmbDeletedData.SelectedItem)
            {
                case "Category":
                    dataGridView1.DataSource = dto.Categories;
                    dataGridView1.Columns[0].Visible = false;
                    dataGridView1.Columns[1].HeaderText = "Category Name";
                    break;
                case "Product":
                    dataGridView1.DataSource = dto.Products;
                    dataGridView1.Columns[0].Visible = false;
                    dataGridView1.Columns[1].HeaderText = "Product Name";
                    dataGridView1.Columns[2].Visible = false;
                    dataGridView1.Columns[3].HeaderText = "Category Name";
                    dataGridView1.Columns[4].HeaderText = "Stock Amount";
                    dataGridView1.Columns[5].HeaderText = "Price";
                    break;
                case "Customer":
                    dataGridView1.DataSource = dto.Customers;
                    dataGridView1.Columns[0].Visible = false;
                    dataGridView1.Columns[1].HeaderText = "Customer Name";
                    break;
                case "Sales":
                    dataGridView1.DataSource = dto.Sales;
                    dataGridView1.Columns[0].Visible = false;
                    dataGridView1.Columns[1].Visible = false;
                    dataGridView1.Columns[2].HeaderText = "Customer";
                    dataGridView1.Columns[3].Visible = false;
                    dataGridView1.Columns[4].HeaderText = "Category";
                    dataGridView1.Columns[5].Visible = false;
                    dataGridView1.Columns[6].HeaderText = "Product";
                    dataGridView1.Columns[7].HeaderText = "Sales Amount";
                    dataGridView1.Columns[8].HeaderText = "Sales Price";
                    dataGridView1.Columns[9].HeaderText = "Stock Amount";
                    dataGridView1.Columns[10].HeaderText = "Date";
                    break;
                default: dataGridView1.DataSource = null; break;
            }
        }

        private void btnGetBack_Click(object sender, EventArgs e)
        {
            if (detail.ID != 0)
            {
                DialogResult result = MessageBox.Show("Are you sure you want to get back this deleted sales?", "Warning!!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                if (result == DialogResult.Yes)
                {
                    if (sbll.GetBack(detail))
                    {
                        MessageBox.Show("Deleted sales information restored.");
                        sbll = new SalesBLL();
                        dto = sbll.Select(true);
                        dataGridView1.DataSource = dto.Sales;
                    }
                }
            }
            else if (detail.CategoryID != 0)
            {
                DialogResult result = MessageBox.Show("Are you sure you want to get back this deleted category?", "Warning!!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                if (result == DialogResult.Yes)
                {
                    CategoryDetailDTO kdetail = new CategoryDetailDTO();
                    kdetail.ID = detail.CategoryID;
                    if (kbll.GetBack(kdetail))
                    {
                        MessageBox.Show("Deleted category information restored.");
                        sbll = new SalesBLL();
                        dto = sbll.Select(true);
                        dataGridView1.DataSource = dto.Categories;
                    }
                }
            }
            else if (detail.CustomerID != 0)
            {
                DialogResult result = MessageBox.Show("Are you sure you want to get back this deleted customer?", "Warning!!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                if (result == DialogResult.Yes)
                {
                    CustomerDetailDTO cdetail = new CustomerDetailDTO();
                    cdetail.ID = detail.CustomerID;
                    if (cbll.GetBack(cdetail))
                    {
                        MessageBox.Show("Deleted customer information restored.");
                        sbll = new SalesBLL();
                        dto = sbll.Select(true);
                        dataGridView1.DataSource = dto.Customers;
                    }
                }
            }
            else if (detail.ProductID != 0)
            {
                DialogResult result = MessageBox.Show("Are you sure you want to get back this deleted customer?", "Warning!!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                if (result == DialogResult.Yes)
                {
                    ProductDetailDTO pdetail = new ProductDetailDTO();
                    pdetail.ID = detail.ProductID;
                    if (pbll.GetBack(pdetail))
                    {
                        MessageBox.Show("Deleted product information restored.");
                        sbll = new SalesBLL();
                        dto = sbll.Select(true);
                        dataGridView1.DataSource = dto.Products;
                    }
                }
            }
        }
    }
}
