﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockTracking.DAL;
using StockTracking.DAL.DTO;

namespace StockTracking.DAL.DAO
{
    public class SalesDAO : StockContext, IDAO<SALE, SalesDetailDTO>
    {
        public bool Delete(SALE entity)
        {
            try
            {
                if (entity.ID != 0)
                {
                    SALE dsale = db.SALES.First(x => x.ID == entity.ID);
                    dsale.DeletedDate = DateTime.Today;
                    dsale.isDeleted = true;
                    db.SaveChanges();
                }
                else if (entity.ProductID != 0)
                {
                    List<SALE> dlsale = new List<SALE>();
                    dlsale = db.SALES.Where(x => x.ProductID == entity.ProductID).ToList();
                    foreach (var item in dlsale)
                    {
                        item.DeletedDate = DateTime.Today;
                        item.isDeleted = true;
                    }
                    db.SaveChanges();
                }
                else if (entity.CustomerID != 0)
                {
                    List<SALE> dlsale = new List<SALE>();
                    dlsale = db.SALES.Where(x => x.CustomerID == entity.CustomerID).ToList();
                    foreach (var item in dlsale)
                    {
                        item.DeletedDate = DateTime.Today;
                        item.isDeleted = true;
                    }
                    db.SaveChanges();
                }
                else if (entity.CategoryID != 0)
                {
                    List<SALE> dlsale = new List<SALE>();
                    dlsale = db.SALES.Where(x => x.CategoryID == entity.CategoryID).ToList();
                    foreach (var item in dlsale)
                    {
                        item.DeletedDate = DateTime.Today;
                        item.isDeleted = true;
                    }
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool GetBack(int ID)
        {
            SALE gbsale = new SALE();
            gbsale = db.SALES.First(x => x.ID == ID);
            gbsale.DeletedDate = null;
            gbsale.isDeleted = false;
            db.SaveChanges();
            return true;
        }

        public bool Insert(SALE entity)
        {
            try
            {
                db.SALES.Add(entity);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<SalesDetailDTO> Select()
        {
            try
            {
                List<SalesDetailDTO> sales = new List<SalesDetailDTO>();
                var list = (from s in db.SALES.Where(x=>x.isDeleted==false)
                           join c in db.CUSTOMERs on s.CustomerID equals c.ID
                           join k in db.CATEGORies on s.CategoryID equals k.ID
                           join p in db.PRODUCTs on s.ProductID equals p.ID
                           select new
                           {
                               id = s.ID,
                               customerId = c.ID,
                               customerName = c.CustomerName,
                               categoryId = k.ID,
                               categoryName = k.CategoryName,
                               productId = p.ID,
                               productName = p.ProductName,
                               salesAmount = s.ProductSalesAmount,
                               salesPrice = s.ProductSalesPrice,
                               stockAmount = p.StockAmount,
                               salesDate = s.SalesDate
                           }).OrderBy(x => x.salesDate);
                foreach (var item in list)
                {
                    SalesDetailDTO dto = new SalesDetailDTO();
                    dto.ID = item.id;
                    dto.CustomerID = item.customerId;
                    dto.CustomerName = item.customerName;
                    dto.CategoryID = item.categoryId;
                    dto.CategoryName = item.categoryName;
                    dto.ProductID = item.productId;
                    dto.ProductName = item.productName;
                    dto.ProductSalesAmount = item.salesAmount;
                    dto.ProductSalesPrice = item.salesPrice;
                    dto.StockAmount = item.stockAmount;
                    dto.SalesDate = item.salesDate;
                    sales.Add(dto);
                }
                return sales;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SalesDetailDTO> Select(bool isDeleted)
        {
            try
            {
                List<SalesDetailDTO> sales = new List<SalesDetailDTO>();
                var list = (from s in db.SALES.Where(x => x.isDeleted == isDeleted)
                            join c in db.CUSTOMERs on s.CustomerID equals c.ID
                            join k in db.CATEGORies on s.CategoryID equals k.ID
                            join p in db.PRODUCTs on s.ProductID equals p.ID
                            select new
                            {
                                id = s.ID,
                                customerId = c.ID,
                                customerName = c.CustomerName,
                                categoryId = k.ID,
                                categoryName = k.CategoryName,
                                productId = p.ID,
                                productName = p.ProductName,
                                salesAmount = s.ProductSalesAmount,
                                salesPrice = s.ProductSalesPrice,
                                stockAmount = p.StockAmount,
                                salesDate = s.SalesDate
                            }).OrderBy(x => x.salesDate);
                foreach (var item in list)
                {
                    SalesDetailDTO dto = new SalesDetailDTO();
                    dto.ID = item.id;
                    dto.CustomerID = item.customerId;
                    dto.CustomerName = item.customerName;
                    dto.CategoryID = item.categoryId;
                    dto.CategoryName = item.categoryName;
                    dto.ProductID = item.productId;
                    dto.ProductName = item.productName;
                    dto.ProductSalesAmount = item.salesAmount;
                    dto.ProductSalesPrice = item.salesPrice;
                    dto.StockAmount = item.stockAmount;
                    dto.SalesDate = item.salesDate;
                    sales.Add(dto);
                }
                return sales;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Update(SALE entity)
        {
            try
            {
                SALE sale = new SALE();
                sale = db.SALES.First(x => x.ID == entity.ID);
                sale.ProductSalesAmount = entity.ProductSalesAmount;
                sale.ProductID = entity.ProductID;
                sale.CustomerID = entity.CustomerID;
                sale.CategoryID = entity.CategoryID;
                sale.ProductSalesPrice = entity.ProductSalesPrice;
                sale.SalesDate = entity.SalesDate;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
