﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StockTracking.BLL;
using StockTracking.DAL.DTO;

namespace StockTracking
{
    public partial class FrmProductList : Form
    {
        public FrmProductList()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = General.isNumber(e);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            FrmProduct frm = new FrmProduct();
            frm.dto = dto;
            this.Hide();
            frm.ShowDialog();
            this.Visible = true;
            FillData();
            CleanData();
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        ProductBLL bll = new ProductBLL();
        ProductDTO dto = new ProductDTO();
        public ProductDetailDTO detail = new ProductDetailDTO();

        void FillData()
        {
            dto = bll.Select();
            dataGridView1.DataSource = dto.Products;
            cmbCategory.DataSource = dto.Categories;
            cmbCategory.DisplayMember = "CategoryName";
            cmbCategory.ValueMember = "ID";
            cmbCategory.SelectedIndex = -1;
        }

        private void FrmProductList_Load(object sender, EventArgs e)
        {
            
            FillData();
            CleanData();
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[1].HeaderText = "Product Name";
            dataGridView1.Columns[2].Visible = false;
            dataGridView1.Columns[3].HeaderText = "Category";
            dataGridView1.Columns[4].HeaderText = "Stock Amount";
            dataGridView1.Columns[5].HeaderText = "Price";
        }

        private void button1_Click(object sender, EventArgs e) //--> Search
        {
            dto = bll.Select();
            if (txtProductName.Text.Trim() != "")
                dto.Products = dto.Products.Where(x => x.ProductName.ToLower().Contains((txtProductName.Text.ToLower()))).ToList();
            if (cmbCategory.SelectedIndex != -1)
                dto.Products = dto.Products.Where(x => x.CategoryID == Convert.ToInt32(cmbCategory.SelectedValue)).ToList();
            if (txtPrice.Text != "")
            {
                if (rbPriceEquals.Checked)
                    dto.Products = dto.Products.Where(x => x.Price == Convert.ToInt32(txtPrice.Text)).ToList();
                else if (rbPriceMore.Checked)
                    dto.Products = dto.Products.Where(x => x.Price > Convert.ToInt32(txtPrice.Text)).ToList();
                else if (rbPriceLess.Checked)
                    dto.Products = dto.Products.Where(x => x.Price < Convert.ToInt32(txtPrice.Text)).ToList();
                else
                    MessageBox.Show("Please select a criterion from price group.");
            }
            if (txtStock.Text != "")
            {
                if (rbStockEquals.Checked)
                    dto.Products = dto.Products.Where(x => x.StockAmount == Convert.ToInt32(txtStock.Text)).ToList();
                else if (rbStockMore.Checked)
                    dto.Products = dto.Products.Where(x => x.StockAmount > Convert.ToInt32(txtStock.Text)).ToList();
                else if (rbStockLess.Checked)
                    dto.Products = dto.Products.Where(x => x.StockAmount < Convert.ToInt32(txtStock.Text)).ToList();
                else
                    MessageBox.Show("Please select a criterion from stock group.");
            }
            dataGridView1.DataSource = dto.Products;
        }

        private void btnClean_Click(object sender, EventArgs e)
        {
            CleanData();
            FillData();
        }

        private void CleanData()
        {
            txtProductName.Clear();
            cmbCategory.SelectedValue = -1;
            txtPrice.Clear();
            rbPriceEquals.Checked = false;
            rbPriceMore.Checked = false;
            rbPriceLess.Checked = false;
            txtStock.Clear();
            rbStockEquals.Checked = false;
            rbStockMore.Checked = false;
            rbStockLess.Checked = false;
        }

        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            detail.ID = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value);
            detail.ProductName = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            detail.CategoryID = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[2].Value);
            detail.CategoryName = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            detail.StockAmount = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[4].Value);
            detail.Price = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[5].Value);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (detail.ID == 0)
                MessageBox.Show("Please select a product from table.");
            else
            {
                FrmProduct frm = new FrmProduct();
                frm.dto = dto;
                frm.detail = detail;
                frm.isUpdate = true;
                this.Hide();
                frm.ShowDialog();
                this.Visible = true;
                FillData();
                CleanData();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (detail.ID == 0)
                MessageBox.Show("Please select a product from table.");
            else
            {
                DialogResult result = MessageBox.Show("Are you sure you want to delete this product?","Warning!!",MessageBoxButtons.YesNo,MessageBoxIcon.Warning,MessageBoxDefaultButton.Button2);
                if (result == DialogResult.Yes)
                {
                    DialogResult result2 = MessageBox.Show("Would you like to delete all sales associated to this product?", "Warning!!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                    if (result2 == DialogResult.Yes)
                        detail.ProductName = "";
                    if (bll.Delete(detail))
                    {
                        if (detail.ProductName == "")
                            MessageBox.Show("Product and associated sales deleted.");
                        else
                            MessageBox.Show("Product was deleted.");
                        bll = new ProductBLL();
                        dto = bll.Select();
                        dataGridView1.DataSource = dto.Products;
                    }
                }
            }
        }
    }
}
